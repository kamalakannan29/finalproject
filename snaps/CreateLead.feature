Feature: Create Lead
Background:
Given Launch the Browser
And Enter the URL as http://leaftaps.com/opentaps
And Enter the UserName as DemoSalesManager
And Enter the Password as crmsfa
And Click on the Login button
And Click on CRMSFA link
And Click on Leads link
And Click on CreateLead link
Scenario Outline: Create a New Lead.
And Enter the FirstName as <fName>
And Enter the LastName as <lName>
And Enter the CompanyName as <cName>
When Click on CreateLead button
Then Verify Lead Creation
Examples: 
|fName|lName|cName|
|kokki|kumar|TCS|
|kumar|kokki|CTS|
