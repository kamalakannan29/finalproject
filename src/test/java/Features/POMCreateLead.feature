Feature: Create Lead 1 

Scenario Outline: Creating a new Lead here
	Given Enter the UserName as DemoSalesManager 
	And Enter the Password as crmsfa 
	And Click on the Login button 
	And Click on CRM SFA link 
	And Click on Leads link 
	And Click on CreateLead link 
	And Enter the FirstName as <fName> 
	And Enter the LastName as <lName> 
	And Enter the CompanyName as <cName> 
	When Click on CreateLead button 
	Then Verify Lead Creation 
	Examples:
		|fName|lName|cName|
		|kokki|kumar|TCS|
		|kumar|kokki|CTS|
		
		
