package RunTest;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@CucumberOptions(
		features="src/test/java",glue= {"Implementation1","pages"}
		,monochrome = true
		/*,dryRun=true
		,snippets=SnippetType.CAMELCASE*/
		)
@RunWith(Cucumber.class)
public class CreateLeadRun {

}
