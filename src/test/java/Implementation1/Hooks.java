package Implementation1;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import pages.SeMethods;

/*public class Hooks {
	@Before
	public void BeforeScenario(Scenario sc) {
		System.out.println("Scenario Name:"+sc.getName() );
		System.out.println("Data fetched:"+sc.getId() );
	}
	
	@After
	public void AfterScenario(Scenario sc) {
		System.out.println("Status of the Scenario ran is:"+sc.getStatus());
	}
}*/

//POM integration
public class Hooks extends SeMethods{
	@Before
	public void BeforeScenario(Scenario sc) {
		startResult();
		testCaseName = sc.getName();
		testDesc = sc.getId();
		category = "Smoke";
		author = "Kamal";
		beforeMethod();
		startApp("chrome","http://leaftaps.com/opentaps");
	}
	
	@After
	public void AfterScenario(Scenario sc) {
		closeAllBrowsers();
		endResult();
	}
}
