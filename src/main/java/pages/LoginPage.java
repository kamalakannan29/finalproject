package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;

public class LoginPage extends ProjectMethods{
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID, using="username") WebElement eleUserName;
	@FindBy(how = How.ID, using="password") WebElement elePassword;
	@FindBy(how = How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogin;
	@Given("Enter the UserName as (.*)")
	public LoginPage enterUserName(String userName) {
		type(eleUserName, userName);
		return this;
	}
	@Given("Enter the Password as (.*)")
	public LoginPage enterPassword(String password) {
		type(elePassword, password);
		return this;
	}
	@Given("Click on the Login button")
	public HomePage clickLogin() {
		click(eleLogin);
		return new HomePage();
	}

}
