package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;

public class HomePage extends ProjectMethods{
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.LINK_TEXT, using="CRM/SFA") WebElement eleCRMSFA;
	@Given("Click on CRM SFA link")
	public MyHomePage clickCRMSFA() {
		click(eleCRMSFA);
		return new MyHomePage();
	}
}
