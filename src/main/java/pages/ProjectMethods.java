package pages;

import java.io.IOException;

import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
//import org.testng.annotations.Parameters;
public class ProjectMethods extends SeMethods {
	//@Parameters({"url","username","password"})
	@BeforeMethod
	//@BeforeMethod(groups="any")
	public void login() {
	//public void login(String url,String username, String password) {
		try {
			beforeMethod();
			//startApp("chrome",url );
			startApp("chrome","http://leaftaps.com/opentaps");
			/*WebElement eleUserName = locateElement("id", "username");
			type(eleUserName,username );
			WebElement elePassword = locateElement("password");
			type(elePassword, password);
			WebElement eleLogin = locateElement("class","decorativeSubmit");
			click(eleLogin);*/
			reportStep("Pass", "Logged into the application is successful");
		}catch (WebDriverException e) {
			reportStep("Fail", "Application login failed");
		}
	}
	@AfterMethod
	//@AfterMethod(groups="any")
	public void closeApp() {
		closeBrowser();
		}
	@DataProvider(name="fetchData")
	public Object[][] getData() throws IOException{
		ReadingExcel data = new ReadingExcel();
		Object[][] sheet = data.readingXL(dataSheetName);
		return sheet;
	}
}
