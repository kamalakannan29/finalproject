package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadPage extends ProjectMethods{
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
@FindBy(how = How.ID, using="createLeadForm_companyName") WebElement eleCompanyName;
@FindBy(how = How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
@FindBy(how = How.ID, using="createLeadForm_lastName") WebElement eleLastName;
@FindBy(how = How.NAME, using="submitButton") WebElement eleCreateLeadSubmit;
	@Given("Enter the CompanyName as (.*)")
	public CreateLeadPage enterCompanyName(String companyName) {
		type(eleCompanyName, companyName);
		return this;
	}
	@Given("Enter the FirstName as (.*)")
	public CreateLeadPage enterFirstName(String firstName) {
		type(eleFirstName, firstName);
		return this;
	}
	@Given("Enter the LastName as(.*)")
	public CreateLeadPage enterLastName(String lastName) {
		type(eleLastName, lastName);
		return this;
	}
	@When("Click on CreateLead button")
	public ViewLeadPage clickCreateLeadSubmit() {
		click(eleCreateLeadSubmit);
		return new ViewLeadPage();
	}
	@Then("Verify Lead Creation")
	public void verifyCreateLead() {
		System.out.println("Created Lead Successfully");
	}
}
