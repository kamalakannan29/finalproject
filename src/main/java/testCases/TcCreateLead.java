package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import pages.ProjectMethods;

public class TcCreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC_Create Lead";
		testDesc = "Creation of New Lead";
		category ="Smoke";
		author = "Kamal";
		dataSheetName = "createLeads";
	}
	@Test(dataProvider = "fetchData")
	public void createLead(String userName, String password, String company, String firstName, String lastName){
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(company)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickCreateLeadSubmit();
	}

}
